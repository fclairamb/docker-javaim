FROM debian:8.3

USER root

# Installing java
RUN \
  echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" > /etc/apt/sources.list.d/webupd8team-java.list && \
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886 && \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  apt-get update && \
  apt-get install oracle-java8-installer -y && \
  rm -rf /var/cache/oracle-jdk8-installer /var/cache/apt/archives/*

# Installing ghostscript + imagemagick
RUN \
  apt-get update && \
  apt-get install ghostscript imagemagick -y && \
  rm -rf /var/cache/apt/archives/*
